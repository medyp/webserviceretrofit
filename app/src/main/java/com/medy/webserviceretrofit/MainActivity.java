package com.medy.webserviceretrofit;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.medy.retrofitwrapper.RequestJSON;
import com.medy.retrofitwrapper.WebRequest;
import com.medy.retrofitwrapper.WebServiceBaseRequestModel;
import com.medy.retrofitwrapper.WebServiceResponseListener;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * @author Manish Kumar
 * @since 29/3/18
 */


public class MainActivity extends AppCompatActivity implements WebServiceResponseListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String url = "http://dummy.restapiexample.com/api/v1/update/15410";
        WebRequest webRequest = new WebRequest(1, url, WebRequest.PUT_REQ);
        webRequest.setRequestModel(new Data());
//        webRequest.addHeader("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZW1iZXJfaWQiOjl9.NwUvL_y2nkFbc5Gq1qHgsPFeF4i4EkaXbQDBpSBOOrM");
        webRequest.send(this);
    }

    public OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        okClientBuilder.connectTimeout(1 * 60 * 1000, TimeUnit.MILLISECONDS);
        okClientBuilder.readTimeout(30 * 1000, TimeUnit.MILLISECONDS);
        okClientBuilder.writeTimeout(15 * 1000, TimeUnit.MILLISECONDS);
        return okClientBuilder.build();
    }


    @Override
    public void onWebRequestCall(WebRequest webRequest) {
//        Data data = webRequest.getResponsePojo(Data.class);
    }

    @Override
    public void onWebRequestResponse(WebRequest webRequest) {
        String responseString = webRequest.getResponseString();
        Log.e("onWebRequestResponse: ", responseString);
    }

    @Override
    public void onWebRequestPreResponse(WebRequest webRequest) {

    }

    @RequestJSON
    public class Data extends WebServiceBaseRequestModel {
        public String name = "Zion";
        public int age = 23;
        public int salary = 12000;
    }
}

package com.medy.retrofitwrapper;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Manish Kumar
 */

@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface RequestJSON {

}

package com.medy.retrofitwrapper;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.HeaderMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * @author Sunil Kumar
 */

public interface WebRequestInterface {

    @HTTP(method = "GET", path = "{api_path}")
    Call<ResponseBody> callGetRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                       @QueryMap Map<String, String> queries,
                                       @HeaderMap Map<String, String> headers);

    @HTTP(method = "POST", path = "{api_path}", hasBody = true)
    Call<ResponseBody> callPostRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                        @QueryMap Map<String, String> queries,
                                        @HeaderMap Map<String, String> headers,
                                        @Body RequestBody multipartBody);

    @HTTP(method = "POST", path = "{api_path}", hasBody = true)
    Call<ResponseBody> callPostRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                        @QueryMap Map<String, String> queries,
                                        @HeaderMap Map<String, String> headers,
                                        @Body JsonObject params);

    @HTTP(method = "PATCH", path = "{api_path}", hasBody = true)
    Call<ResponseBody> callPatchRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                        @QueryMap Map<String, String> queries,
                                        @HeaderMap Map<String, String> headers,
                                        @Body JsonObject params);

    @HTTP(method = "PATCH", path = "{api_path}", hasBody = true)
    Call<ResponseBody> callPatchRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                        @QueryMap Map<String, String> queries,
                                        @HeaderMap Map<String, String> headers,
                                        @Body RequestBody multipartBody);

    @HTTP(method = "PUT", path = "{api_path}", hasBody = true)
    Call<ResponseBody> callPutRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                         @QueryMap Map<String, String> queries,
                                         @HeaderMap Map<String, String> headers,
                                         @Body JsonObject params);

    @HTTP(method = "PUT", path = "{api_path}", hasBody = true)
    Call<ResponseBody> callPutRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                        @QueryMap Map<String, String> queries,
                                        @HeaderMap Map<String, String> headers,
                                        @Body RequestBody multipartBody);

    @HTTP(method = "DELETE", path = "{api_path}")
    Call<ResponseBody> callDeleteRequest (@Path(value = "api_path", encoded = true) String apiPath,
                                          @QueryMap Map<String, String> queries,
                                          @HeaderMap Map<String, String> headers);

}

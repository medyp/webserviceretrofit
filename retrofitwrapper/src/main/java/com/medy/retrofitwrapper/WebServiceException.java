package com.medy.retrofitwrapper;

/**
 * @author Manish Kumar
 */


public class WebServiceException extends RuntimeException {

    public static final int EXCEPTION_IN_CALL = 121;
    public static final int INTERNET_NOT_AVAILABLE = 122;
    int code = 0;

    public WebServiceException(String message) {
        super(message);
    }

    public WebServiceException(String message, int code) {
        super(message);
        this.code = code;
    }

    @Override
    public String toString () {
        return "WebServiceException code=" + code + ", message=" + getMessage();
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}

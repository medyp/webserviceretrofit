package com.medy.retrofitwrapper;

/**
 * @author Manish Kumar
 */
public interface WebServiceProgressResponseListener extends WebServiceResponseListener {


    void onWebRequestProgressChange(WebRequest webRequest);


}

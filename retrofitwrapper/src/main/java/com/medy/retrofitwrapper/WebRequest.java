package com.medy.retrofitwrapper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Sink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Manish Kumar
 * @since 28/8/17
 */


public class WebRequest extends BaseModel {

    static BackGroundThreadExecutor backGroundThreadExecutor;
    static MainThreadExecutor mainThreadExecutor;

    public boolean isDebug = false;

    public static final String LINE_SEPARATOR = "\n";

    /**
     * Request type post
     */
    public static final String POST_REQ = "POST";
    /**
     * Request type get
     */
    public static final String GET_REQ = "GET";
    /**
     * Request type delete
     */
    public static final String DELETE_REQ = "DELETE";

    /**
     * Request type patch
     */
    public static final String PATCH_REQ = "PATCH";

    /**
     * Request type patch
     */
    public static final String PUT_REQ = "PUT";

    public static final long DEFAULT_TIME_OUT = (long) (0.12 * 60 * 1000);


    final long timeout;
    final HashMap<String, String> webRequestParams = new LinkedHashMap<>();
    final HashMap<String, String> webRequestHeaders = new LinkedHashMap<>();
    final HashMap<String, String> webRequestQuery = new LinkedHashMap<>();
    final HashMap<String, File> webRequestFiles = new LinkedHashMap<>();
    final HashMap<String, Object> extraDataMap = new LinkedHashMap<>();
    JsonObject webServiceRequestBody;
    final int webRequestId;
    final String webRequestUrl;
    final String webRequestMethod;
    final TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[]{};
                }
            }
    };
    Context context;
    String username;
    String password;
    Certificate certificate;
    WebRequestInterface webRequestInterface;
    String webRequestResponse;
    int responseCode = -1;
    HashSet<String> cookies;
    HashSet<String> requestCookies;
    boolean isCancelled;
    Call<ResponseBody> call;
    Exception webRequestException;
    WebRequestErrorDialog webRequestErrorDialog;
    ProgressDialog progressDialog;
    String progressMessage = "";
    private ConnectionDetector cd;
    Handler progressHandler = new Handler(Looper.getMainLooper());
    long totalLength;
    long uploadedLength;

    Object responsePojo;

    public HashMap<String, String> getWebRequestHeaders() {
        return webRequestHeaders;
    }

    public HashMap<String, String> getWebRequestParams() {
        return webRequestParams;
    }

    public HashMap<String, String> getWebRequestQuery() {
        return webRequestQuery;
    }


    public Object getResponsePojo() {
        return responsePojo;
    }

    public void setResponsePojo(Object responsePojo) {
        this.responsePojo = responsePojo;
    }

    public long getTotalLength() {
        return totalLength;
    }

    public long getUploadedLength() {
        return uploadedLength;
    }

    public WebRequest(int webRequestId, String webRequestUrl) {
        this(webRequestId, webRequestUrl, GET_REQ);
    }

    public WebRequest(int webRequestId, String webRequestUrl,
                      String webRequestMethod) {
        this(webRequestId, webRequestUrl, webRequestMethod, DEFAULT_TIME_OUT);

    }


    public WebRequest(int webRequestId, String webRequestUrl,
                      String webRequestMethod, long timeout) {
        this.webRequestId = webRequestId;
        this.webRequestMethod = webRequestMethod;
        this.timeout = timeout;

        Uri uri = Uri.parse(webRequestUrl);
        parseQueryParams(uri);

        String portString = "";
        if (uri.getPort() != -1) {
            portString = ":" + String.valueOf(uri.getPort());
        }
        this.webRequestUrl = uri.getScheme() + "://" + uri.getHost() + portString + uri.getPath();

        if (this.backGroundThreadExecutor == null) {
            this.backGroundThreadExecutor = new BackGroundThreadExecutor();
            this.backGroundThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
        if (this.mainThreadExecutor == null) {
            this.mainThreadExecutor = new MainThreadExecutor();
            this.mainThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    }

    public static BackGroundThreadExecutor getBackGroundThreadExecutor() {
        return backGroundThreadExecutor;
    }

    public static MainThreadExecutor getMainThreadExecutor() {
        return mainThreadExecutor;
    }

    private void parseQueryParams(Uri uri) {
        webRequestQuery.clear();
        if (uri.isOpaque()) {
            return;
        }

        String query = uri.getEncodedQuery();
        if (query == null) {
            return;
        }


        int start = 0;
        do {
            int next = query.indexOf('&', start);
            int end = (next == -1) ? query.length() : next;

            int separator = query.indexOf('=', start);
            if (separator > end || separator == -1) {
                separator = end;
            }

            String name = query.substring(start, separator);
            String value;
            if (separator < end)
                value = query.substring(separator + 1, end);
            else
                value = "";

            webRequestQuery.put(Uri.decode(name), Uri.decode(value));

            // Move start to end of name.
            start = end + 1;
        } while (start < query.length());
    }


    public void setRequestModel(WebServiceBaseRequestModel webServiceBaseRequestModel) {
        webRequestHeaders.clear();
        webRequestParams.clear();
        webRequestFiles.clear();
        if (webServiceBaseRequestModel != null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            Gson gson = gsonBuilder.create();

            boolean asJsonRequest = webServiceBaseRequestModel.getClass().isAnnotationPresent(RequestJSON.class);
            if (asJsonRequest) {
                this.webServiceRequestBody = new JsonParser().parse(gson.toJson(webServiceBaseRequestModel)).getAsJsonObject();
                return;
            }

            for (Field f : webServiceBaseRequestModel.getClass().getFields()) {
                f.setAccessible(true);
                try {
                    String key = f.getName();
                    if (key.equals("serialVersionUID")) {
                        continue;
                    }
                    Object value = f.get(webServiceBaseRequestModel);
                    boolean isHeader = f.isAnnotationPresent(WebServiceHeader.class);
                    if (value != null) {
                        if (isHeader) {
                            addHeader(key, ((String) value));
                            continue;
                        }
                        boolean asJSON = f.isAnnotationPresent(ParamJSON.class);
                        if (f.getType() == List.class) {
                            List dataList = (List) value;
                            if (asJSON) {
                                addParam(key, gson.toJson(dataList));
                            } else {
                                int i = 0;
                                for (Object data : dataList) {
                                    String newKey = key + "[" + i + "]";
                                    if (data instanceof File) {
                                        addFile(newKey, (File) data);
                                    } else {
                                        addParam(newKey, String.valueOf(data));
                                    }
                                    i++;
                                }
                            }
                        } else if (f.getType() == Object.class) {
                            if (value instanceof File) {
                                addFile(key, (File) value);
                            } else {
                                addParam(key, String.valueOf(value));
                            }
                        } else if (f.getType() == File.class) {
                            addFile(key, (File) value);
                        } else {
                            if (asJSON) {
                                addParam(key, gson.toJson(value));
                            } else {
                                addParam(key, String.valueOf(value));
                            }
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void printLog(String msg) {
        if (isDebug) {
            Log.e(getClass().getSimpleName() + webRequestId, msg);
        }
    }

    private boolean isDebugBuild(Context context) {

        try {
            String packageName = context.getPackageName();

            Bundle bundle = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA).metaData;
            String manifest_pkg = null;
            if (bundle != null) {
                manifest_pkg = bundle.getString("manifest_pkg", null);
            }
            if (manifest_pkg != null) {
                packageName = manifest_pkg;
            }

            final Class<?> buildConfig = Class.forName(packageName + ".BuildConfig");
            final Field DEBUG = buildConfig.getField("DEBUG");
            DEBUG.setAccessible(true);
            return DEBUG.getBoolean(null);
        } catch (final Throwable t) {
            final String message = t.getMessage();
            if (message != null && message.contains("BuildConfig")) {
                // Proguard obfuscated build. Most likely a production build.
                return false;
            } else {
                return BuildConfig.DEBUG;
            }
        }
    }


    public int getWebRequestId() {
        return webRequestId;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void cancel() {
        if (this.call != null) {
            if (!this.call.isCanceled())
                this.call.cancel();
        }
    }

    private void onRequestCompleted(Exception e, Response<ResponseBody> response) {
        int responseCode = -1;
        HashSet<String> cookies = null;
        String webRequestResponse = null;
        if (response != null) {
            responseCode = response.code();
            okhttp3.Response raw = response.raw();
            if (!raw.headers("Set-Cookie").isEmpty()) {
                cookies = new HashSet<>();
                for (String header : raw.headers("Set-Cookie")) {
                    cookies.add(header);
                }
            }
            try {
                if (response.isSuccessful() && response.body() != null) {
                    webRequestResponse = response.body().string().trim();
                } else if (response.errorBody() != null) {
                    webRequestResponse = response.errorBody().string().trim();
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        onRequestCompleted(e, responseCode, webRequestResponse, cookies);
    }

    public void onRequestCompleted(Exception e, int responseCode, String webRequestResponse,
                                   HashSet<String> cookies) {
        this.webRequestException = e;
        this.responseCode = responseCode;
        this.cookies = cookies;
        this.webRequestResponse = webRequestResponse;
    }

    public void setBasicAuth(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void addParam(String key, String value) {
        webRequestParams.put(key, value);
    }

    public void addHeader(String key, String value) {
        webRequestHeaders.put(key, value);
    }

    public void addQuery(String key, String value) {
        webRequestQuery.put(key, value);
    }

    public void clearQuery() {
        webRequestQuery.clear();
    }

    public void addFile(String key, File file) {
        webRequestFiles.put(key, file);
    }

    public void addExtra(String key, Object value) {
        extraDataMap.put(key, value);
    }

//    public Object getExtraData (String key) {
//        return extraDataMap.get(key);
//    }

    public <T> T getExtraData(String key) {
        return (T) extraDataMap.get(key);
    }

    public static MultipartBody.Part createFilePart(String variableName, String filePath, MediaType mediaType) {
        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(mediaType, file);
        return MultipartBody.Part.createFormData(variableName, file.getName(), requestFile);
    }

    private Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    private Call<ResponseBody> generateRequest(Context context, final WebServiceResponseListener webServiceResponseListener) {
        String basicAuth = getBasicAuth();
        if (basicAuth != null) {
            addHeader("Authorization", basicAuth);
        }
        if (requestCookies != null) {
            for (String cookie : requestCookies) {
                addHeader("Cookie", cookie);
            }
        }


        Call<ResponseBody> call = null;
        WebRequestInterface webRequestInterface = getWebServiceInterface(getCertificate());
        if (this.webRequestMethod.equals(DELETE_REQ)) {
            call = webRequestInterface.callDeleteRequest(this.webRequestUrl,
                    webRequestQuery, webRequestHeaders);
        } else if (this.webRequestMethod.equals(GET_REQ)) {

            call = webRequestInterface.callGetRequest(this.webRequestUrl,
                    webRequestQuery, webRequestHeaders);

        } else if (this.webRequestMethod.equals(POST_REQ) ||
                this.webRequestMethod.equals(PATCH_REQ) ||
                this.webRequestMethod.equals(PUT_REQ)) {

            if (webServiceRequestBody != null) {
                addHeader("Content-Type", "application/json");
                if (this.webRequestMethod.equals(POST_REQ)) {
                    call = webRequestInterface.callPostRequest(this.webRequestUrl, webRequestQuery,
                            webRequestHeaders,
                            webServiceRequestBody);
                } else if (this.webRequestMethod.equals(PATCH_REQ)) {
                    call = webRequestInterface.callPatchRequest(this.webRequestUrl, webRequestQuery,
                            webRequestHeaders,
                            webServiceRequestBody);
                } else if (this.webRequestMethod.equals(PUT_REQ)) {
                    call = webRequestInterface.callPutRequest(this.webRequestUrl, webRequestQuery,
                            webRequestHeaders,
                            webServiceRequestBody);
                }
                return call;
            }

            MultipartBody.Builder builder = new MultipartBody.Builder();
            if (!webRequestParams.isEmpty()) {
                for (String key : webRequestParams.keySet()) {
                    if (key == null) continue;
                    String value = webRequestParams.get(key);
                    if (value == null) continue;
                    builder.addFormDataPart(key, value);
                }
            }
            if (!webRequestFiles.isEmpty()) {
                for (String key : webRequestFiles.keySet()) {
                    if (key == null) continue;
                    final File value = webRequestFiles.get(key);
                    if (value == null) continue;
                    final String mimeType = getMimeType(value.getAbsolutePath());
                    if (mimeType == null) continue;
                    RequestBody requestBody = new RequestBody() {
                        @Override
                        public MediaType contentType() {
                            return MediaType.parse(mimeType);
                        }

                        @Override
                        public long contentLength() {
                            return value.length();
                        }

                        @Override
                        public void writeTo(BufferedSink sink) throws IOException {
                            byte[] buffer = new byte[2048];
                            FileInputStream in = new FileInputStream(value);
                            try {
                                int read;
                                while ((read = in.read(buffer)) != -1) {
                                    sink.write(buffer, 0, read);
                                    WebRequest.this.uploadedLength += read;
                                    if (webServiceResponseListener != null &&
                                            (webServiceResponseListener instanceof WebServiceProgressResponseListener)) {
                                        ((WebServiceProgressResponseListener) webServiceResponseListener).onWebRequestProgressChange(WebRequest.this);
                                        printProgressResponseLog();
                                    }
                                }
                            } finally {
                                in.close();
                            }


                        }
                    };
                    builder.addFormDataPart(key, value.getName(), requestBody);
                }
            }
            MultipartBody body = builder.build();
            try {
                WebRequest.this.totalLength = body.contentLength();
                addHeader("Content-Type", "multipart/form-data; boundary=" + body.boundary());
                if (this.webRequestMethod.equals(POST_REQ)) {
                    call = webRequestInterface.callPostRequest(this.webRequestUrl, webRequestQuery,
                            webRequestHeaders,
                            body);
                } else if (this.webRequestMethod.equals(PATCH_REQ)) {
                    call = webRequestInterface.callPatchRequest(this.webRequestUrl, webRequestQuery,
                            webRequestHeaders,
                            body);
                } else if (this.webRequestMethod.equals(PUT_REQ)) {
                    call = webRequestInterface.callPutRequest(this.webRequestUrl, webRequestQuery,
                            webRequestHeaders,
                            body);
                }

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


        }
        return call;
    }


    private String getBasicAuth() {
        if (this.username == null || this.password == null) return null;
        String basicAuth = "Basic " +
                Base64.encodeToString(String.format("%s:%s", username, password)
                                .getBytes(),
                        Base64.NO_WRAP);
        return basicAuth;
    }

    private String getMimeType(String filePath) {
        String mimeType = null;
        Uri uri = Uri.fromFile(new File(filePath));
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            if (fileExtension != null && !fileExtension.trim().isEmpty()) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        fileExtension.toLowerCase());
            }
        }
        return mimeType;
    }

    private String getWebRequestFullUrl() {
        StringBuilder builder = new StringBuilder(this.webRequestUrl);
        if (!webRequestQuery.isEmpty()) {
            builder.append("?");
            Set<Map.Entry<String, String>> entrySet = webRequestQuery.entrySet();
            int i = 0;
            for (Map.Entry<String, String> entry : entrySet) {
                if (i > 0) {
                    builder.append("&");
                }
                builder.append(entry.getKey()).append("=").append(entry.getValue());
                i++;
            }
        }
        return builder.toString();
    }

    public String printRequestLog() {
        if (!isDebug) return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("webRequestId:").append(webRequestId).append(LINE_SEPARATOR);
        stringBuilder.append("webRequestUrl:").append(getWebRequestFullUrl()).append(LINE_SEPARATOR);

        stringBuilder.append("webRequestMethod:").append(webRequestMethod).append(LINE_SEPARATOR);
        if (this.username != null && this.password != null) {
            stringBuilder.append("username:").append(username).append(LINE_SEPARATOR);
            stringBuilder.append("password:").append(password).append(LINE_SEPARATOR);
        }
        if (!webRequestHeaders.isEmpty()) {
            stringBuilder.append("Headers=>").append(LINE_SEPARATOR);
            for (Map.Entry<String, String> data : webRequestHeaders.entrySet()) {
                stringBuilder.append(data.getKey() + ":" + data.getValue()).append(LINE_SEPARATOR);
            }
        }
        if (!webRequestParams.isEmpty()) {
            stringBuilder.append("Parameters=>").append(LINE_SEPARATOR);
            for (Map.Entry<String, String> data : webRequestParams.entrySet()) {
                stringBuilder.append(data.getKey() + ":" + data.getValue()).append(LINE_SEPARATOR);
            }
        }
        if (!webRequestFiles.isEmpty()) {
            stringBuilder.append("FileParameters=>").append(LINE_SEPARATOR);
            for (Map.Entry<String, File> data : webRequestFiles.entrySet()) {
                stringBuilder.append(data.getKey() + ":" + data.getValue().getAbsolutePath())
                        .append(", MIME:").append(getMimeType(data.getValue().getAbsolutePath())).append(LINE_SEPARATOR);
            }
        }
        if (webServiceRequestBody != null) {
            stringBuilder.append("webServiceRequestBody=>").append(LINE_SEPARATOR);
            stringBuilder.append(webServiceRequestBody).append(LINE_SEPARATOR);
        }

        if (requestCookies != null) {
            stringBuilder.append("requestCookies=>").append(LINE_SEPARATOR);
            stringBuilder.append(requestCookies).append(LINE_SEPARATOR);
        }
        printLog(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public String printResponseLog() {
        if (!isDebug) return "";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("webRequestId:").append(webRequestId).append(LINE_SEPARATOR);
        stringBuilder.append("webRequestUrl:").append(getWebRequestFullUrl()).append(LINE_SEPARATOR);
        stringBuilder.append("webRequestMethod:").append(webRequestMethod).append(LINE_SEPARATOR);
        if (this.username != null && this.password != null) {
            stringBuilder.append("username:").append(username).append(LINE_SEPARATOR);
            stringBuilder.append("password:").append(password).append(LINE_SEPARATOR);
        }
        if (webRequestException != null) {
            stringBuilder.append("webRequestException:").append(webRequestException.toString()).append(LINE_SEPARATOR);
        }
        if (webRequestResponse != null) {
            String result = getResponseString();
            stringBuilder.append("responseCode:").append(responseCode).append(LINE_SEPARATOR);
            stringBuilder.append("result:").append(result).append(LINE_SEPARATOR);
            if (cookies != null) {
                stringBuilder.append("receivedCookies:").append(cookies).append(LINE_SEPARATOR);
            }

        }
        printLog(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public void printProgressResponseLog() {
        if (!isDebug) return;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("webRequestId:").append(webRequestId).append(LINE_SEPARATOR);
        stringBuilder.append("webRequestUrl:").append(getWebRequestFullUrl()).append(LINE_SEPARATOR);
        stringBuilder.append("---------progress -----------").append(LINE_SEPARATOR);
        stringBuilder.append("uploadedLength:" + uploadedLength).append(LINE_SEPARATOR);
        stringBuilder.append("totalLength:" + totalLength).append(LINE_SEPARATOR);
        stringBuilder.append("percent:" + (((float) uploadedLength) / totalLength) * 100).append(LINE_SEPARATOR);
        stringBuilder.append("-----------------------------").append(LINE_SEPARATOR);
        printLog(stringBuilder.toString());
    }

    public void setProgressMessage(String progressMessage) {
        this.progressMessage = progressMessage;
    }

    public void showProgressDialog() {
        if (progressDialog != null &&
                progressMessage != null && !progressMessage.trim().isEmpty()) {
            progressDialog.setMessage(progressMessage);
            progressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public String getResponseString() {
        if (webRequestResponse != null) {
            return webRequestResponse;
        }
        return "";
    }

    public Exception getWebRequestException() {
        return webRequestException;
    }

    public WebServiceBaseResponseModel getBaseResponsePojo() throws JsonSyntaxException {
        String response = getResponseString();
        if (response != null && !response.trim().isEmpty()) {
            WebServiceBaseResponseModel baseWebServiceModel = new Gson().fromJson(response, WebServiceBaseResponseModel.class);
            return baseWebServiceModel;

        }
        return null;
    }

    public boolean isSuccess() {
        if (webRequestException == null) {
            try {
                WebServiceBaseResponseModel baseWebServiceModel = getBaseResponsePojo();
                if (baseWebServiceModel != null && !baseWebServiceModel.isError()) {
                    return true;
                }
            } catch (JsonSyntaxException e) {
                showInvalidResponse(getResponseString());
            }

        } else {
            if (webRequestException.getMessage() != null)
                showInvalidResponse(webRequestException.getMessage());
        }
        return false;
    }

    public boolean checkSuccess() {
        if (webRequestException == null) {
            try {
                WebServiceBaseResponseModel baseWebServiceModel = getBaseResponsePojo();
                if (baseWebServiceModel != null && !baseWebServiceModel.isError()) {
                    return true;
                }
            } catch (JsonSyntaxException e) {

            }
        }
        return false;
    }


    public String getErrorMessageFromResponse() {
        try {
            WebServiceBaseResponseModel webServiceBaseResponseModel = getBaseResponsePojo();
            if (webServiceBaseResponseModel != null && webServiceBaseResponseModel.isError()) {
                return webServiceBaseResponseModel.getMessage();
            }
        } catch (JsonSyntaxException e) {

        }
        if (webRequestException != null && webRequestException.getMessage() != null) {
            return webRequestException.getMessage();
        }
        return "";
    }


    public <T> T getResponsePojo(Class<T> tClass) {

        String response = getResponseString();
        if (response != null && !response.trim().isEmpty()) {
            try {
                return (new Gson().fromJson(response, tClass));
            } catch (JsonSyntaxException ignore) {
                showInvalidResponse(response);
            }
        }
        return null;
    }

    public <T> List<T> getResponsePojoList(TypeToken typeToken) {

        String response = getResponseString();
        if (response != null && !response.trim().isEmpty()) {
            try {
                return (new Gson().fromJson(response, typeToken.getType()));
            } catch (JsonSyntaxException e) {
                showInvalidResponse(response);
            }
        }
        return null;
    }

    public <T> T getResponsePojoSimple(Class<T> tClass) {

        String response = getResponseString();
        if (response != null && !response.trim().isEmpty()) {
            try {
                return (new Gson().fromJson(response, tClass));
            } catch (JsonSyntaxException ignore) {

            }
        }
        return null;
    }

    public <T> List<T> getResponsePojoSimpleList(TypeToken typeToken) {

        String response = getResponseString();
        if (response != null && !response.trim().isEmpty()) {
            try {
                return (new Gson().fromJson(response, typeToken.getType()));
            } catch (JsonSyntaxException ignore) {

            }
        }
        return null;
    }


    public int getResponseCode() {
        return responseCode;
    }

    public HashSet<String> getCookies() {
        return cookies;
    }

    public void setRequestCookies(HashSet<String> cookies) {
        this.requestCookies = cookies;
    }

    public void showInvalidResponse(String msg) {
        if (context == null) return;
        if (webRequestErrorDialog != null && webRequestErrorDialog.isShowing()) {
            webRequestErrorDialog.dismiss();
        }
        if (context instanceof Activity) {
            webRequestErrorDialog = new WebRequestErrorDialog(context, msg);
            webRequestErrorDialog.show();
        }
    }

    public void showToast(String message) {
        if (context == null || message == null || message.trim().isEmpty()) return;
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public Call<ResponseBody> generateCall(Context context) throws WebServiceException {
        isDebug = isDebugBuild(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);

        this.context = context;
        if (this.cd == null)
            cd = new ConnectionDetector(context);
        this.call = generateRequest(context, null);
        isCancelled = this.call.isCanceled();
        printRequestLog();
        if (!cd.isConnectingToInternet()) {
            String msg = "Please check your internet connection.";
            WebServiceException e = new WebServiceException(msg,
                    WebServiceException.INTERNET_NOT_AVAILABLE);
            onRequestCompleted(e, null);
            printResponseLog();
            throw e;
        }
        return this.call;
    }

    public void send(Context context) {
        if (!(context instanceof WebServiceResponseListener)) {
            throw new IllegalArgumentException("context should be implement WebServiceResponseListener");
        }
        this.send(context, ((WebServiceResponseListener) context));
    }

    public void send(Context context, final WebServiceResponseListener webServiceResponseListener) {
        isDebug = isDebugBuild(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);

        this.context = context;
        if (this.cd == null)
            cd = new ConnectionDetector(context);
        try {
            webServiceResponseListener.onWebRequestCall(this);
            this.call = generateRequest(context, webServiceResponseListener);
            isCancelled = this.call.isCanceled();
            printRequestLog();
            if (cd.isConnectingToInternet()) {
                showProgressDialog();
                try {
                    this.call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            isCancelled = call.isCanceled();
                            onRequestCompleted(null, response);
                            printResponseLog();
                            webServiceResponseListener.onWebRequestPreResponse(WebRequest.this);

                            mainThreadExecutor.handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressDialog();
                                    webServiceResponseListener.onWebRequestResponse(WebRequest.this);
                                }
                            });

                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            isCancelled = call.isCanceled();
                            String message = (t != null ? t.getMessage() : "");
                            onRequestCompleted(new IllegalStateException(message), null);
                            printResponseLog();
                            webServiceResponseListener.onWebRequestPreResponse(WebRequest.this);

                            mainThreadExecutor.handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressDialog();
                                    webServiceResponseListener.onWebRequestResponse(WebRequest.this);
                                }
                            });
                        }
                    });
                } catch (Exception e) {
                    String msg = e.getMessage() == null ? "Exception in call webRequest" : e.getMessage();
                    WebServiceException e1 = new WebServiceException(msg,
                            WebServiceException.EXCEPTION_IN_CALL);
                    onRequestCompleted(e1, null);
                    printResponseLog();
                    mainThreadExecutor.handler.post(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            webServiceResponseListener.onWebRequestResponse(WebRequest.this);
                        }
                    });
                }

            } else {
                String msg = "Please check your internet connection.";
                WebServiceException e = new WebServiceException(msg,
                        WebServiceException.INTERNET_NOT_AVAILABLE);
                onRequestCompleted(e, null);
                printResponseLog();
                mainThreadExecutor.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        webServiceResponseListener.onWebRequestResponse(WebRequest.this);
                    }
                });
            }
        } catch (Exception e) {
            String msg = e.getMessage() == null ? "Exception in call webRequest" : e.getMessage();
            WebServiceException e1 = new WebServiceException(msg,
                    WebServiceException.EXCEPTION_IN_CALL);
            onRequestCompleted(e1, null);
            printResponseLog();
            mainThreadExecutor.handler.post(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();
                    webServiceResponseListener.onWebRequestResponse(WebRequest.this);
                }
            });
        }

    }


    private WebRequestInterface getWebServiceInterface(Certificate certificate) {
        if (webRequestInterface == null) {

            OkHttpClient client = getOkHttpClient(certificate);
            webRequestInterface = new Retrofit.Builder()
                    .baseUrl("http://www.test.com/")
                    .client(client)
                    .callbackExecutor(backGroundThreadExecutor)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(WebRequestInterface.class);
        }
        return webRequestInterface;
    }


    public void setWebRequestInterface(WebRequestInterface webRequestInterface) {
        this.webRequestInterface = webRequestInterface;
    }

    public OkHttpClient getOkHttpClient(Certificate certificate) {
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();

        if (certificate != null) {
            // Create an ssl socket factory with our all-trusting manager
            try {
                SSLSocketFactory sslSocketFactory = getSSLConfig(certificate).getSocketFactory();
                if (sslSocketFactory != null)
                    okClientBuilder.sslSocketFactory(sslSocketFactory);
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }


        okClientBuilder.connectTimeout(timeout, TimeUnit.MILLISECONDS);
        okClientBuilder.readTimeout(timeout, TimeUnit.MILLISECONDS);
        okClientBuilder.writeTimeout(timeout, TimeUnit.MILLISECONDS);
//
//        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
//            @Override
//            public void log (String message) {
//                Log.e("RETROFIT : ", message);
//            }
//        });
//        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        okClientBuilder.addInterceptor(httpLoggingInterceptor);

        return okClientBuilder.build();
    }

    private SSLContext getSSLConfig(Certificate certificate) throws CertificateException,
            IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        if (certificate == null) return null;
        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        // creating a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", certificate);

        // creating a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        // creating an SSLSocketFactory that uses our TrustManager
        SSLContext sslContext = SSLContext.getInstance("TLS");
        //        sslContext.init(null, tmf.getTrustManagers(), null);
        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

        return sslContext;
    }


    final class CountingSink extends ForwardingSink {

        private long bytesWritten = 0;
        private long contentLength = 0;

        CountingSink(Sink delegate, long contentLength) {
            super(delegate);
            this.contentLength = contentLength;
        }

        @Override
        public void write(Buffer source, long byteCount) throws IOException {
            super.write(source, byteCount);
            bytesWritten += byteCount;
        }

        public long getContentLength() {
            return contentLength;
        }

        public long getBytesWritten() {
            return bytesWritten;
        }
    }


    static class BackGroundThreadExecutor implements Executor {

        private Handler mHandler = null;

        private HandlerThread mHandlerThread = null;


        public BackGroundThreadExecutor() {
            HandlerThread mHandlerThread = new HandlerThread("BackGroundThreadExecutor");
            mHandlerThread.start();
            mHandler = new Handler(mHandlerThread.getLooper());
        }

        @Override
        public void execute(Runnable command) {
            mHandler.post(command);
        }

    }

    static class MainThreadExecutor implements Executor {
        private final Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable r) {
            handler.post(r);
        }
    }


}

package com.medy.retrofitwrapper;

/**
 * @author Manish Kumar
 */
public interface WebServiceResponseListener {

    void onWebRequestCall (WebRequest webRequest);

    /**
     * This Method is call from @ {@link }
     * If WebService call successfully then this method is call on main thread
     */
    void onWebRequestResponse (WebRequest webRequest);


    /**
     * This Method is call from @ {@link }
     * If WebService call successfully then this method is call on background thread
     */
    void onWebRequestPreResponse (WebRequest webRequest);



}

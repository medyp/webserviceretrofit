package com.medy.retrofitwrapper;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Manish Kumar
 */

@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface WebServiceHeader {

}

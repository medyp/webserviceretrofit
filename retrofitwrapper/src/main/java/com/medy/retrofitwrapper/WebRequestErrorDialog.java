package com.medy.retrofitwrapper;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


/**
 * @author Manish Kumar
 */


public class WebRequestErrorDialog extends Dialog {

    TextView tv_message;
    TextView tv_dismiss_btn;
    Handler handler = new Handler() {
        @Override
        public void handleMessage (Message msg) {
            super.handleMessage(msg);
            try {
                if (isShowing())
                    dismiss();
            } catch (Exception e) {

            }
        }
    };

    String msg = "";

    public WebRequestErrorDialog (Context context, String msg) {
        super(context);
        this.msg = msg;
    }

    public int getLayoutResourceId () {
        return R.layout.dialog_webrequest_response_invalid;
    }

    public int getDialogAnimationStyleId () {
        return R.style.LoginProcessDialog;
    }

    public float getDimAmount () {
        return 0.0f;
    }

    public int getMessageTextViewId () {
        return R.id.tv_message;
    }

    public int getDismissBtnTextViewId () {
        return R.id.tv_dismiss_btn;
    }

    public int getGravity () {
        return Gravity.BOTTOM;
    }

    public int getBackGroundDrawableResource () {
        return R.drawable.bg_error_dialog;
    }

    public boolean isCancelable () {
        return true;
    }

    public boolean isCancelOnOutSide () {
        return true;
    }

    public boolean isAutoCancel () {
        return true;
    }

    public long getAutoCancelDuration () {
        return 3000;
    }


    public void setMsg (String msg) {
        this.msg = msg;
    }


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(getBackGroundDrawableResource());
        LayoutInflater inflate = LayoutInflater.from(getContext());
        View layout = inflate.inflate(getLayoutResourceId(), null);
        setContentView(layout);
        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.windowAnimations = getDialogAnimationStyleId();
        wlmp.gravity = getGravity();
        wlmp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlmp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlmp.dimAmount = getDimAmount();
        setTitle(null);
        setCancelable(isCancelable());
        setCanceledOnTouchOutside(isCancelOnOutSide());
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss (DialogInterface dialog) {
                handler.removeMessages(1);
            }
        });
        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel (DialogInterface dialog) {
                handler.removeMessages(1);
            }
        });
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow (DialogInterface dialog) {
                if (isAutoCancel()) {
                    handler.sendEmptyMessageDelayed(1, getAutoCancelDuration());
                }
                if (WebRequestErrorDialog.this.msg != null) {
                    tv_message.setText(Html.fromHtml(WebRequestErrorDialog.this.msg));
                } else {
                    tv_message.setText("");
                }
            }
        });
        tv_message = layout.findViewById(getMessageTextViewId());
        tv_dismiss_btn = layout.findViewById(getDismissBtnTextViewId());
        tv_dismiss_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if (WebRequestErrorDialog.this.isShowing()) {
                    dismiss();
                }
            }
        });

    }

}
